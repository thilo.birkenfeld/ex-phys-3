# Programmieraufgaben zur Experimentalphysik III - Optik un Quantenphysik (WS 23)

(Mit Rechtsklick "Show Markdown Preview" wir dieses File in formatierter Version angezeigt)

1. Navigator\
In [index.ipynb](./index.ipynb) finden sie einen Navigator zu den ausgegebenen Programmierübungen und veröffentlichten Lösungen sowie zu den Einstiegstutorials.

2. Übungen\
Sie können auch ohne den Navigator direkt auf die Programmieraufgaben zugreifen. Diese sind ab gegebener Zeit im Unterordner [Übungen](./Übungen) zu finden.\
Leider können die Übungen **nicht direkt über JupyterLab eingereicht werden** und müssen zusammen mit den übrigen Übungen **über Moodle eingereicht** werden.

3. Python Module\
Neben den *Python3* builtin Modulen stehen ihnen für die Bearbeitung der Aufgaben einige Standardmodule zur Datenverarbeitung zur Verfügung:
    1. NumPy
    2. SciPy
    3. SymPy
    4. matplotlib
    5. seaborn
    6. bokeh


4. Kontakt\
Falls Fragen bezüglich der Programmieraufgaben oder zu den bereit gestellten Modulen auftreten bitte melden sie sich bei mir: <birkenfeld@physik.rwth-aachen.de>